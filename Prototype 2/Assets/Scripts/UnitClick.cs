using UnityEngine;

public class UnitClick : MonoBehaviour
{

    private Camera myCam;
    public GameObject groundMarker;

    public LayerMask clickable;
    public LayerMask ground;

    void Start()
    {
        myCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {

            RaycastHit hit;
            Ray ray = myCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, clickable))
            {

                // if we hit a clickable object
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    //shift click
                    UnitSelections.Instance.ShiftClickSelect(hit.collider.gameObject);
                }

                else
                {
                    //normal click
                    UnitSelections.Instance.ClickSelect(hit.collider.gameObject);
                }


            }
            else
            {
                // if we don't hit a clickable object & not shift clicking

                if (!Input.GetKey(KeyCode.LeftShift))
                {
                    UnitSelections.Instance.DeselectAll();
                }

            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = myCam.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray, out hit, Mathf.Infinity, ground))
            {
                groundMarker.transform.position = hit.point;
                groundMarker.SetActive(false);
                groundMarker.SetActive(true);

            }
        }

    }
}
